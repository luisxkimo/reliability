<!--
Please review https://about.gitlab.com/handbook/engineering/infrastructure/change-management/ for the most recent information on our change plans and execution policies.
-->

# Migrate large projects off {+ file-XX-stor-gprd +}

### Summary

Migrate the large projects currently on `file-XX-stor-gprd.c.gitlab-production.internal` ([storage stats](https://dashboards.gitlab.net/d/W_Pbu9Smk/storage-stats?var-env=gprd&var-environment=gprd&orgId=1&refresh=30m&viewPanel=10&var-node=file-XX)) to `file-YY-stor-gprd.c.gitlab-production.internal` ([storage stats](https://dashboards.gitlab.net/d/W_Pbu9Smk/storage-stats?var-env=gprd&var-environment=gprd&orgId=1&refresh=30m&viewPanel=10&var-node=file-YY)).

#### Why?

The Git repository storage shard file server disk usage is high: current shard disk usage is at {+ shard-usage +} of capacity as of {+ date time UTC +}.

Several other file storage nodes have more than 50% capacity available, so instead of additional node creation, a re-balance of git repositories between shards is indicated.

#### Runbook

This change follows the procedure described in the [GitLab Storage Re-balancing runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/gitaly/storage-rebalancing.md).

### Change Details

1. **Services Impacted**  - ~"Service:Gitaly"
1. **Change Technician**  - <!-- woodhouse: '`@{{ .Username }}`' -->{+ DRI for the execution of this change +}
1. **Change Reviewer**    - <!-- woodhouse: '@{{ .Reviewer }}' -->{+ DRI for the review of this change +}
1. **Time tracking**      - <!-- woodhouse: '{{ .Duration}}' -->{+ Time, in minutes, needed to execute all change steps, including rollback +}

## Detailed steps for the change

### Pre-Change Steps - steps to be completed before execution of the change

*Estimated Time to Complete (mins)* - {+Estimated Time to Complete in Minutes+}

- [ ] Replace all occurrences of `XX` with the source gitaly shard node number
- [ ] Replace all occurrences of `YY` with the destination gitaly shard node number if you're migrating to a specific shard. Alternatively, you can ommit the `YY` input in the script and the script will automatically find a shard with available storage.
- [ ] Set label ~"change::in-progress" on this issue
- [ ] Install the [re-balance script](https://gitlab.com/gitlab-com/runbooks/blob/master/scripts/storage_rebalance.rb) on a system that has access to the gitaly shard systems, ideally your local workstation:
  - [ ] Clone the runbooks repository:
    ```sh
    git clone git@gitlab.com:gitlab-com/runbooks.git
    ```
  - [ ] Change directory into the cloned runbooks repository and install the Ruby dependencies
    ```sh
    cd runbooks
    bundle config set --local path vendor/bundle
    bundle install
    ```
  - [ ] Confirm that the storage re-balance script can be run:
    ```sh
    bundle exec scripts/storage_rebalance.rb --help
    ```
  - [ ] Confirm that the cleanup script can be run:
    ```sh
    bundle exec scripts/storage_cleanup.rb --help
    ```
- [ ] Create a Personal Access Token with the `api` scope enabled, then export it as an environment variable in your shell session:
  ```sh
  export GITLAB_GPRD_ADMIN_API_PRIVATE_TOKEN=your-token
  ```
- [ ] Execute a dry-run of the re-balance script to move files from `nfs-fileXX` to `nfs-fileYY` and post the results in a comment in this issue;
      do not give an amount of disk space to migrate (`--move-amount N`), so that only one single project will be migrated for the time being:
  ```sh
  bundle exec scripts/storage_rebalance.rb nfs-fileXX nfs-fileYY --verbose --wait=10800 --max-failures=1 --dry-run=yes
  ```
- [ ] Verify that the dry-run execution behaved as expected (no errors).

### Change Steps - steps to take to execute the change

*Estimated Time to Complete (mins)* - {+Estimated Time to Complete in Minutes+}

- [ ] Take a snapshot of the disk for `nfs-fileXX` and post the snapshot name in a comment in this issue.
 
  If you have installed `gcloud` on your system, then the following commands may be used in a shell session:
  ```sh
  disk_name='file-XX-stor-gprd-data'
  gcloud auth login
  gcloud config set project gitlab-production
  zone=$(gcloud compute disks list --filter="name=('${disk_name}')" --format=json | jq -r '.[0]["zone"]' | cut -d'/' -f9)
  echo "${zone}"
  snapshot_name=$(gcloud compute disks snapshot "${disk_name}" --zone="${zone}" --format=json | jq -r '.[0]["name"]')
  echo "${snapshot_name}"
  gcloud compute snapshots list --filter="name=('${snapshot_name}')" --format=json | jq -r '.[0]["status"]'
  ```

- [ ] Execute the re-balance script to move files from `nfs-fileXX` to `nfs-fileYY`;
      specify an amount of disk space in gigabytes (less than `16000`) to migrate. Say, 1000 gigabytes, for example:
  ```sh
  bundle exec scripts/storage_rebalance.rb nfs-fileXX nfs-fileYY --verbose --wait=10800 --max-failures=20 --move-amount=1000 --dry-run=no | tee scripts/logs/nfs-fileXX.migration.$(date +%Y-%m-%d_%H%M).log
  ```

### Post-Change Steps - steps to take to verify the change

*Estimated Time to Complete (mins)* - {+Estimated Time to Complete in Minutes+}

- [ ] Verify that the execution behaved as expected. If there were errors, that is alright because the failed repository replications are logged and can be examined using this command:
  ```sh
  find scripts/storage_migrations -name "failed_projects_*" -exec jq . {} \;
  ```
- [ ] Take a final snapshot of the disk for `nfs-fileXX` and post the snapshot name in a comment in this issue.
 
  If you have installed `gcloud` on your system, then the following commands may be used in a shell session:
  ```sh
  disk_name='file-XX-stor-gprd-data'
  gcloud auth login
  gcloud config set project gitlab-production
  zone=$(gcloud compute disks list --filter="name=('${disk_name}')" --format=json | jq -r '.[0]["zone"]' | cut -d'/' -f9)
  echo "${zone}"
  snapshot_name=$(gcloud compute disks snapshot "${disk_name}" --zone="${zone}" --format=json | jq -r '.[0]["name"]')
  echo "${snapshot_name}"
  gcloud compute snapshots list --filter="name=('${snapshot_name}')" --format=json | jq -r '.[0]["status"]'
  ```
- [ ] Execute a dry-run of the cleanup script and post the results in a comment in this issue:
  ```sh
  bundle exec scripts/storage_cleanup.rb file-XX-stor-gprd.c.gitlab-production.internal --verbose --scan --dry-run=yes
  ```
- [ ] Request a review from another SRE of the output of the dry-run execution
- [ ] Finally, execute the cleanup script:
  ```sh
  bundle exec scripts/storage_cleanup.rb file-XX-stor-gprd.c.gitlab-production.internal --verbose --scan --dry-run=no
  ```

## Rollback

### Rollback steps - steps to be taken in the event of a need to rollback this change

*Estimated Time to Complete (mins)* - {+Estimated Time to Complete in Minutes+}

- [ ] Take a snapshot of the disk for `nfs-fileYY` and post the snapshot name in a comment in this issue.
 
  If you have installed `gcloud` on your system, then the following commands may be used in a shell session:
  ```sh
  disk_name='file-YY-stor-gprd-data'
  gcloud auth login
  gcloud config set project gitlab-production
  zone=$(gcloud compute disks list --filter="name=('${disk_name}')" --format=json | jq -r '.[0]["zone"]' | cut -d'/' -f9)
  echo "${zone}"
  snapshot_name=$(gcloud compute disks snapshot "${disk_name}" --zone="${zone}" --format=json | jq -r '.[0]["name"]')
  echo "${snapshot_name}"
  gcloud compute snapshots list --filter="name=('${snapshot_name}')" --format=json | jq -r '.[0]["status"]'
  ```
- [ ] Execute a dry-run of the cleanup script and post the results in a comment in this issue:
  ```sh
  bundle exec scripts/storage_cleanup.rb file-YY-stor-gprd.c.gitlab-production.internal --verbose --scan --dry-run=yes
  ```
- [ ] Request a review from another SRE of the output of the dry-run execution
- [ ] Finally, execute the cleanup script:
  ```sh
  bundle exec scripts/storage_cleanup.rb file-YY-stor-gprd.c.gitlab-production.internal --verbose --scan --dry-run=no
  ```

## Monitoring

### Key metrics to observe

<!--
  * Describe which dashboards and which specific metrics we should be monitoring related to this change using the format below.
-->

- Metric: Gitaly Per-Node Service Aggregated SLIs Apdex
  - Location:
    - [gitaly: Host Detail - file-XX](https://dashboards.gitlab.net/d/gitaly-host-detail/gitaly-host-detail?orgId=1&var-environment=gprd&var-fqdn=file-XX-stor-gprd.c.gitlab-production.internal)
    - [gitaly: Host Detail - file-YY](https://dashboards.gitlab.net/d/gitaly-host-detail/gitaly-host-detail?orgId=1&var-environment=gprd&var-fqdn=file-YY-stor-gprd.c.gitlab-production.internal)
  - What changes to this metric should prompt a rollback: an apdex drop below 99.5%

## Change Reviewer checklist

- [ ] The **scheduled day and time** of execution of the change is appropriate.
- [ ] The [change plan](#detailed-steps-for-the-change) is technically accurate.
- [ ] The change plan includes **estimated timing values** based on previous testing.
- [ ] The change plan includes a viable [rollback plan](#rollback).
- [ ] The specified [metrics/monitoring dashboards](#key-metrics-to-observe) provide sufficient visibility for the change.

## Change Technician checklist

<!--
To find out who is on-call, in #production channel run: /chatops run oncall production.
-->

- [ ] This issue has a criticality label (~C3) and a change-type label (e.g. ~"change::unscheduled", ~"change::scheduled") based on the [Change Management Criticalities](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#change-criticalities).
- [ ] This issue has the change technician as the assignee.
- [ ] Pre-Change, Change, Post-Change, and Rollback steps and have been filled out and reviewed.
- [ ] This Change Issue is linked to the appropriate Issue and/or Epic
- [ ] Necessary approvals have been completed based on the [Change Management Workflow](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#change-request-workflows).
- [ ] Change has been tested in staging and results noted in a comment on this issue.
- [ ] A dry-run has been conducted and results noted in a comment on this issue.
- [ ] SRE on-call has been informed prior to change being rolled out. (In #production channel, mention `@sre-oncall` and this issue and await their acknowledgement.)
- [ ] There are currently no [active incidents](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Incident%3A%3AActive).

/label ~infrastructure ~C3 ~change ~toil ~"Service:Gitaly" ~"requires production access"
