# Nix SRE Module

## Options

```nix
# Keep me a secret, see NixOS.md
config.gitlab = {
    email        = "me@gitlab.com";
    # Get me with: sudo nix-shell -p dmidecode --command "dmidecode -s system-serial-number"
    serialNumber = "FTWWZXH"; 
    # ITSecurity have a special token that you'll need to pass in here
    sentinelOneManagementToken = "";
}
```

## What does this do?
Sets up your local development environment with all the tools and services needed for a typical SRE workflow. See [here](./default.nix) for a complete list of things installed.


## ERD Components
Both drivestrike and sentinel-one:
 - Have hardcoded configuration paths
 - Mutate their own configuration

So it's a little tricky to keep them entirely hermetic, you could chroot then mount /proc etc if you were so inclined, right now we're just dumping it on the root filesystem, to be sure that they are working correctly, once we find a way to confirm that they are indeed working correctly then we can remove the bootstrapping step.

Until then you'll need to run `sudo bootstrap-sentinelone` post install, which essentially copies the opt/ directory from the sentinelone derivation to the root /opt

## TL;DR
Follow steps in [NixOS](NixOS.md)
```
# --impure is required because of nix secrets 
# being outside of the git respository

sudo nixos-rebuild switch --impure
sudo bootstrap-sentinelone
```