{ config, lib, ... }:
with lib;
let
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/f28c957a927b0d23636123850f7ec15bda9aa2f4.tar.gz") {
    config = {
      allowUnfreePredicate = pkg: builtins.elem (pkgs.lib.getName pkg) [
        "1password-cli"
      ];
    };
  };
  # Calliope\ gitlab-com/gl-infra/jsonnet-tool is required for building the runbooks repo
  # Here we've globalized it for ease of use but it could absolutely be in a shell.nix
  # inside the repo itself.
  jsonnet-tool = (pkgs.buildGoModule rec {
    pname = "jsonnet-tool";
    version = "1.0.0";
    src = pkgs.fetchFromGitLab {
      owner = "gitlab-com/gl-infra";
      repo  = "jsonnet-tool";
      rev   = "e650ec91";
      sha256 = "sha256-hZWOdMYgxnV1WjCvWe9yxhEK/unPfe5ntrSLCubhdMQ="; 
    };
    vendorSha256 = "sha256-6j2OdROxj23SzvuVWk7iFFDVgfWqBgrz54fvLuSb5cs=";
  });
in {
  options = {
    gitlab = {
      email = mkOption {
        type = types.str;
        example = "me@gitlab.com";
      };
      serialNumber = mkOption {
        type = types.str;
        example = "FTXYZWW";
      };
      sentinelOneManagementToken = mkOption {
        type = types.str;
        example = "eyxxxxyyyyzzz";
      };

    };
  };

  imports = [
    ./pkgs/drivestrike
    ./pkgs/sentinelone
  ];

  # Calliope\ For yubikey
  config.services.pcscd.enable = true;

  # Calliope\ For ERD
  config.services.drivestrike.enable = true;
  config.services.sentinel-one.enable = true;

  # Calliope\ Environment setup!
  config.environment = {
    sessionVariables = {
      # Calliope\ I use kubectl to proxy vault
      # TODO: parameterize this.
      VAULT_ADDR = "https://localhost:8200";
      VAULT_TLS_SERVER_NAME = "vault.ops.gke.gitlab.net";
    };

    # Calliope\ Bit ol' list of all the stuff we use on a regular basis
    systemPackages = with pkgs; [
      google-cloud-sdk
      chefdk
      teleport
      (pkgs.mkTerraform {
        version = "1.3.7";
        sha256 = "sha256-z49DXJ9oYObJQWHPeuKvQ6jJtAheYuy0+QmvZ74ZbTQ=";
        vendorSha256 = "sha256-fviukVGBkbxFs2fJpEp/tFMymXex7NRQdcGIIA9W88k=";
        patches = [ ./patches/provider-path-0_15.patch ];
      })
      doctl
      awscli2
      kubernetes-helm
      minikube
      vagrant
      sshuttle
      jq
      kubectx
      kubectl
      jsonnet-tool
      _1password
      vault-bin

      # Calliope\ We'll use vscodium-fhs here so we can use extensions in a normal way 
      vscodium-fhs

      # Calliope\ These are required for yubikey setup
      yubikey-personalization
      libu2f-host
      pcsctools
    ];
  };
}
